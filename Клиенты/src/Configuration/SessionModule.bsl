
Процедура УстановкаПараметровСеанса(ТребуемыеПараметры)
	ТекПользовательИБ = ПользователиИнформационнойБазы.ТекущийПользователь();
	Если ЗначениеЗаполнено(ТекПользовательИБ.Имя) Тогда
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	Пользователи.Ссылка КАК Ссылка
		               |ИЗ
		               |	Справочник.Пользователи КАК Пользователи
		               |ГДЕ
		               |	Пользователи.ГУИД = &ГУИД";
		Запрос.УстановитьПараметр("ГУИД",ТекПользовательИБ.УникальныйИдентификатор);
		Выборка = Запрос.Выполнить().Выбрать();
		Если Выборка.Следующий() Тогда
			ТекПользователь = Выборка.Ссылка;
		Иначе
			НовыйПользователь = Справочники.Пользователи.СоздатьЭлемент();
			НовыйПользователь.Наименование = ТекПользовательИБ.Имя;
			НовыйПользователь.ПолноеИмя = ТекПользовательИБ.ПолноеИмя;
			НовыйПользователь.ГУИД = ТекПользовательИБ.УникальныйИдентификатор;
			НовыйПользователь.Записать();
			ТекПользователь = НовыйПользователь.Ссылка;
		КонецЕсли;	
		ПараметрыСеанса.ТекущийПользователь = ТекПользователь;
	КонецЕсли;	
КонецПроцедуры
